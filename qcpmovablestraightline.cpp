#include "qcpmovablestraightline.h"
#include <iostream>
#include "qcustomplotwithroi.h"


QCPMovableStraightLine::QCPMovableStraightLine(QCustomPlotWithROI* parentPlot):
    QCPItemStraightLine(reinterpret_cast<QCustomPlot*>(parentPlot)),
    mParentPlotwROI(parentPlot),
    flagMoving(false)
{
    point1_startOfMove.setX(0);
    point1_startOfMove.setY(0);
    diff_P1ToP2_startOfMove.setX(0);
    diff_P1ToP2_startOfMove.setY(0);
}

//QCPMovableStraightLine::~QCPMovableStraightLine()
//{
//}


void QCPMovableStraightLine::mouseMoveEvent(QMouseEvent* event, const QPointF& startPos)
{
    if (flagMoving)
    {
        diff_P1ToP2_startOfMove = point2->coords() - point1->coords();

        //transform startPos as well as event->localPos() into plot coordinates using the link to
        //the parent QCPwithROI, which is the entity that knows about the relationship between widget pixels
        //and axis coordinates
        double eventKey, eventValue = 0;
        mParentPlotwROI->colorMap->pixelsToCoords(event->localPos(), eventKey, eventValue);
        QPointF eventLocalPos(eventKey, eventValue);

        double startPosKey, startPosValue = 0;
        mParentPlotwROI->colorMap->pixelsToCoords(startPos, startPosKey, startPosValue);
        QPointF startPosCoords(startPosKey, startPosValue);
        // calculate the position of point1 with respect to the start point of the moveEvent
        // posDifference = posP1@endMove - posMouse@endMove = posP1@startMove - posMouse@startMove
        // hence posP1@endMove = posMouse@endMove + posDifference
        QPointF coordsDifference_moveStartToPoint1Begin = point1_startOfMove - startPosCoords;
        QPointF point1_new = parentPlot()->getNextPixelCoords(eventLocalPos + coordsDifference_moveStartToPoint1Begin);
        double Xstep = parentPlot()->singleCellStepX();
        double Ystep = parentPlot()->singleCellStepY();
        if (point1_new.x()<parentPlot()->colorMap->data()->keyRange().upper+Xstep && point1_new.x()>parentPlot()->colorMap->data()->keyRange().lower-Xstep
                && point1_new.y()<parentPlot()->colorMap->data()->valueRange().upper+Ystep && point1_new.y()>parentPlot()->colorMap->data()->valueRange().lower-Ystep)
        {
            point1->setCoords(point1_new);
        }
        //point1->setCoords(eventLocalPos + coordsDifference_moveStartToPoint1Begin);
        //set point2 at the same distance relative to point1 as at the start
        QPointF point2_new = parentPlot()->getNextPixelCoords(point1->coords() + diff_P1ToP2_startOfMove);
        point2->setCoords(point2_new);
    }
    emit(lineMoved());
}


void QCPMovableStraightLine::mousePressEvent(QMouseEvent* event, const QVariant& details)
{
    if(event->button()==Qt::LeftButton)
    {
        std::cout << "mouse press for movable line";
        if (point1->coords().x() == point2->coords().x())
            std::cout << " vertical" << std::endl;
        else if (point1->coords().y() == point2->coords().y()) {
            std::cout << " horizontal" << std::endl;
        }
        point1_startOfMove.setX(point1->coords().x());
        point1_startOfMove.setY(point1->coords().y());
        flagMoving = true;
    }
    //event->ignore();
}

void QCPMovableStraightLine::mouseReleaseEvent(QMouseEvent *event, const QPointF &startPos)
{
    if(event->button()==Qt::LeftButton)
        flagMoving = false;
}
