#ifndef QCPRECTROI_H
#define QCPRECTROI_H
#include "qcustomplot.h"

//class forward declaration
class QCustomPlotWithROI;

class QCPRectROI : public QCPItemRect
{
    Q_OBJECT

public:
    static QPointF label_offset;

    QCustomPlotWithROI* mParentPlotwROI;
    int ROIid;
    QCPItemText* sizeLabel;
    QCPItemLine* VLine;
    QCPItemLine* HLine;

    QCPRectROI(QCustomPlotWithROI* parentPlot);
    virtual ~QCPRectROI();
    QCustomPlotWithROI* parentPlot() const {return mParentPlotwROI;}

    QPointF topLeftPos_startOfMove;
    inline double sizeX(){return (bottomRight->coords().x() - topLeft->coords().x());}
    inline double sizeY(){return (bottomRight->coords().y() - topLeft->coords().y());}

    void mousePressEvent(QMouseEvent* event, const QVariant& details);
    void mouseMoveEvent(QMouseEvent* event, const QPointF& startPos);
    //void selectEvent(QMouseEvent* event, bool additive, const QVariant& details, bool* selectionStateChanged);

signals:
    void rectangleMoved();
    void rectangleSizeChange();

public slots:
    void sizeChangeFromWheel(QWheelEvent* event, bool vertical, bool bigStep);


};

#endif // QCPRECTROI_H
