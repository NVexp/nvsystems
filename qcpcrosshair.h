#ifndef QCPCROSSHAIR_H
#define QCPCROSSHAIR_H
#include "qcustomplot.h"
#include "qcpmovablestraightline.h"

#include <QObject>

class QCPCrosshair
{
public:
    QCPCrosshair(QCustomPlot* parentPlot);

    QCPItemTracer* intersection;
    QCPMovableStraightLine* lineVertical;
    QCPMovableStraightLine* lineHorizontal;

    //virtual double selectTest(const QPointF& pos, bool onlySelectable, QVariant* details = 0) const;
    //virtual void draw(QCPPainter* painter);

};

#endif // QCPCROSSHAIR_H
