#include "qcustomplotwithroi.h"
#include <iostream>

void print_QPointF(QPointF p)
{
    std::cout << "x=" << p.x() << ", y=" << p.y() << std::endl;
}

QCustomPlotWithROI::QCustomPlotWithROI(QWidget* parentWidget): QCustomPlot(parentWidget), lineH(nullptr), lineV(nullptr)
{
    colorMap = new QCPColorMap(xAxis, yAxis);
    setInteractions(QCP::iRangeZoom|QCP::iSelectItems|QCP::iRangeDrag);

    stagePosition = new QCPItemTracer(this);
    QPen crossPen;
    crossPen.setWidth(2);
    crossPen.setColor(Qt::green);
    stagePosition->setPen(crossPen);
    stagePosition->setVisible(false);
    stagePosition->setStyle(QCPItemTracer::TracerStyle::tsPlus);

    YouAreHere = new QCPItemText(this);
    YouAreHere->setText("You're here !");
    YouAreHere->setColor(Qt::green);
    YouAreHere->setVisible(false);
    YouAreHere->setSelectable(false);
    YouAreHere->position->setType(QCPItemPosition::ptAbsolute);
}

QCustomPlotWithROI::~QCustomPlotWithROI()
{
    while(ROIStore.size()>0)
    {
        removeROI();
    }
    removeItem(stagePosition);
    removeItem(YouAreHere);
}

//returns the length of one cell along X in the color map
double QCustomPlotWithROI::singleCellStepX()
{
    double Xcell1, Xcell0 = 0;
    colorMap->data()->cellToCoord(1, 0, &Xcell1, 0 );
    colorMap->data()->cellToCoord(0, 0, &Xcell0, 0);
    return (Xcell1 - Xcell0);
}

//returns the length of one cell along Y in the color map
double QCustomPlotWithROI::singleCellStepY()
{
    double Ycell1, Ycell0 = 0;
    colorMap->data()->cellToCoord(0, 1, 0, &Ycell1);
    colorMap->data()->cellToCoord(0, 0, 0, &Ycell0);
    return (Ycell1 - Ycell0);
}

QPointF QCustomPlotWithROI::getNextPixelCoords(QPointF point)
{
    double px_X = singleCellStepX();
    double xMin = colorMap->data()->keyRange().lower;
    double intX = round((point.x() - xMin)/px_X);
    double px_Y = singleCellStepY();
    double yMin = colorMap->data()->valueRange().lower;
    double intY = round((point.y() - yMin)/px_Y);
    return QPointF{xMin + px_X*intX, px_Y*intY + yMin};
}

void QCustomPlotWithROI::addROI(QPointF ROICenter, int sizeX, int sizeY)
{
    //dynamically construct a new ROI
    QCPRectROI* ROI = new QCPRectROI(this);
    // QCPItemPosition should be ptPlotCoords by default but we want to make it really clear & explicit here !!
    ROI->topLeft->setType(QCPItemPosition::ptPlotCoords);
    ROI->bottomRight->setType(QCPItemPosition::ptPlotCoords);

    int sizeXodd = sizeX + (1-sizeX%2)*1;
    int sizeYodd = sizeY + (1-sizeY%2)*1;

    //define a vector that spans one quarter of the area of the ROI (half x_size * half y_size)
    QPointF quarterROI{((double)sizeXodd/2.)*singleCellStepX(), ((double)sizeYodd/2.)*singleCellStepY()};

    ROI->topLeft->setCoords(ROICenter - quarterROI);
    ROI->bottomRight->setCoords(ROICenter + quarterROI);

    //add the ROI* to the vector storing all of them
    ROIStore.push_back(ROI);

    ROI->ROIid = ROIStore.size();
    std::cout << "ROIid:" << ROI->ROIid << std::endl;

    //add a doted vertical and horizontal line to the ROI that cross in the middle of it
    ROI->HLine->start->setParentAnchor(ROI->top);
    ROI->HLine->end->setParentAnchor(ROI->bottom);
    ROI->HLine->setSelectable(false);
    ROI->HLine->setPen(Qt::DotLine);
    ROI->VLine->start->setParentAnchor(ROI->left);
    ROI->VLine->end->setParentAnchor(ROI->right);
    ROI->VLine->setPen(Qt::DotLine);
    ROI->VLine->setSelectable(false);

    //round convert the sizes of the ROI to integers
    //then setup the size label for the ROI
    int sizeXint = roundToInt(ROI->sizeX()/singleCellStepX());
    int sizeYint = roundToInt(ROI->sizeY()/singleCellStepY());
    std::string sizestr = "ROI " + std::to_string(ROI->ROIid) + ": " + std::to_string(sizeXint) + ", " + std::to_string(sizeYint);
    ROI->sizeLabel->setText(QString::fromStdString(sizestr));
    ROI->sizeLabel->setSelectable(false);
    ROI->sizeLabel->setColor(Qt::white);

    ROI->sizeLabel->position->setType(QCPItemPosition::ptAbsolute);
    QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
    ROI->sizeLabel->position->setCoords(sizepos);

    connect(ROI, SIGNAL(rectangleMoved()), this, SLOT(replot()));
    connect(ROI, SIGNAL(rectangleSizeChange()), this, SLOT(replot()));
}


void QCustomPlotWithROI::removeROI()
{
    if (ROIStore.size() > 0)
    {
        bool success = removeItem(ROIStore[ROIStore.size()-1]);
        if(success)
            ROIStore.removeLast();
        else
            std::cerr << "Problem removing ROI!" << std::endl;
    }
}


void QCustomPlotWithROI::enableCrosshair()
{
    lineH = new QCPMovableStraightLine(this);
    lineV = new QCPMovableStraightLine(this);
    QPen linePen;
    linePen.setWidth(1);
    linePen.setStyle(Qt::DashLine);
    lineH->setPen(linePen);
    lineV->setPen(linePen);
    lineH->setSelectable(false);
    lineV->setSelectable(false);
    // QCPItemPosition should be ptPlotCoords by default but we want to make it really clear & explicit here !!
    lineH->point1->setType(QCPItemPosition::ptPlotCoords);
    lineH->point2->setType(QCPItemPosition::ptPlotCoords);
    lineV->point1->setType(QCPItemPosition::ptPlotCoords);
    lineV->point2->setType(QCPItemPosition::ptPlotCoords);


    double keyMiddle = 0.5*(colorMap->data()->keyRange().lower + colorMap->data()->keyRange().upper);
    double valueMiddle = 0.5*(colorMap->data()->valueRange().lower + colorMap->data()->valueRange().upper);
    double keyRange = colorMap->data()->keyRange().upper - colorMap->data()->keyRange().lower;
    double valueRange = colorMap->data()->valueRange().upper - colorMap->data()->valueRange().lower;
    QPointF P1H{keyMiddle - keyRange/4., valueMiddle};
    QPointF P2H{keyMiddle + keyRange/4., valueMiddle};
    QPointF P1V{keyMiddle, valueMiddle - valueRange/4.};
    QPointF P2V{keyMiddle, valueMiddle + valueRange/4.};
    lineH->point1->setCoords(P1H);
    lineH->point2->setCoords(P2H);
    lineV->point1->setCoords(P1V);
    lineV->point2->setCoords(P2V);

    connect(lineV, SIGNAL(lineMoved()), this, SLOT(replot()));
    connect(lineH, SIGNAL(lineMoved()), this, SLOT(replot()));

    replot();
}

void QCustomPlotWithROI::disableCrosshair()
{
    removeItem(lineH);
    lineH = nullptr;
    removeItem(lineV);
    lineV = nullptr;
    replot();
}


void QCustomPlotWithROI::keyPressEvent(QKeyEvent* event)
{
    int keyPressed = event->key();
    switch(keyPressed)
    {
        case Qt::Key_Control:
            flag_VertSizeChange = true;
        break;
        case Qt::Key_Shift:
            flag_BigStep = true;
        break;
    }
}

void QCustomPlotWithROI::keyReleaseEvent(QKeyEvent *event)
{
    int keyPressed = event->key();
    switch(keyPressed)
    {
        case Qt::Key_Control:
            flag_VertSizeChange = false;
        break;
        case Qt::Key_Shift:
            flag_BigStep = false;
        break;
    }
}

//void QCustomPlotWithROI::scanROIsAndSizeChange(QWheelEvent *event)
void QCustomPlotWithROI::wheelEvent(QWheelEvent* event)
{
    bool must_rescale = true;
    for (auto ROI : ROIStore)
    {
        if (ROI->selected())
        {
            must_rescale = must_rescale && false;
            ROI->sizeChangeFromWheel(event, flag_VertSizeChange, flag_BigStep);
        }
    }


    if (must_rescale)
    {
        //I want to keep the functionality of the base class QCustomPlot so I can
        //just call the event handler from the base class ! it is that simple !!
        QCustomPlot::wheelEvent(event);
        //keep position of "you are here !" sign with respect to ROI
        if (YouAreHere->visible())
        {
            QPointF here = stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
            YouAreHere->position->setCoords(here);
        }
        //keep position of size label with respect to the ROI
        for (auto ROI : ROIStore)
        {
            QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
            ROI->sizeLabel->position->setCoords(sizepos);
        }
        replot();
        return;
    }
    replot();
}

void QCustomPlotWithROI::mouseMoveEvent(QMouseEvent* event)
{
    //keep position of "you are here !" sign with respect to ROI
    if (YouAreHere->visible())
    {
        QPointF here = stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
        YouAreHere->position->setCoords(here);
    }
    //keep position of size label with respect to the ROI
    for (auto ROI : ROIStore)
    {
        QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
        ROI->sizeLabel->position->setCoords(sizepos);
    }

    //eventually keep the functionality of the base class QCustomPlot
    QCustomPlot::mouseMoveEvent(event);

}

QPointF QCustomPlotWithROI::getROIposition(int ROIid)
{
    return 0.5*(ROIStore[ROIid-1]->topLeft->coords() + ROIStore[ROIid-1]->bottomRight->coords());
}

void QCustomPlotWithROI::goToROI(int ROIid)
{
    if (!(stagePosition->visible()))
    {
        stagePosition->setVisible(true);
    }
    QPointF ROIpos = getROIposition(ROIid);
    stagePosition->position->setCoords(ROIpos);

    //set the position of the "You are here !" sign
    QPointF here = stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
    YouAreHere->position->setCoords(here);
    YouAreHere->setVisible(true);

    replot();

    //emit signals to talk to the LCD number
    emit(posMovedToX(ROIpos.x()));
    emit(posMovedToY(ROIpos.y()));
}
