#ifndef QCUSTOMPLOTWITHROI_H
#define QCUSTOMPLOTWITHROI_H
#include <QWidget>
#include "qcustomplot.h"
#include "qcprectroi.h"
#include "qcpcrosshair.h"
#include "iostream"

class QCPRectROI;

inline int roundToInt(double d) {return (int)(d+.5);}


typedef struct XYstagePos{
    int X;
    int Y;
} stagePos;

class QCustomPlotWithROI : public QCustomPlot
{
    Q_OBJECT
public:
    QCustomPlotWithROI(QWidget* parentWidget);
    virtual ~QCustomPlotWithROI();

    inline int ROInumber(){return ROIStore.size();}

    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    void wheelEvent(QWheelEvent* event);
    void mouseMoveEvent(QMouseEvent* event);

    double singleCellStepX();
    double singleCellStepY();
    QPointF getNextPixelCoords(QPointF point);
    QPointF getROIposition(int ROIid);

    QCPItemTracer* stagePosition;
    QCPItemText* YouAreHere;
    QCPColorMap* colorMap;
    QCPMovableStraightLine* lineH;
    QCPMovableStraightLine* lineV;

    QVector<QCPRectROI*> ROIStore;

private:
    bool flag_BigStep;
    bool flag_VertSizeChange;

signals:
    void posMovedToX(double Xpos);
    void posMovedToY(double Ypos);

public slots:
    void addROI(QPointF ROICenter, int sizeX, int sixeY);
    void removeROI();
    void goToROI(int ROIid);

    void enableCrosshair();
    void disableCrosshair();
};

#endif // QCUSTOMPLOTWITHROI_H
