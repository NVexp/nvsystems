#include "qcprectroi.h"
#include "qcustomplotwithroi.h"

#include <iostream>

//class QCustomPlotWithROI;
//need to declare AGAIN and define the static member here
QPointF QCPRectROI::label_offset = QPointF{0,10};


QCPRectROI::QCPRectROI(QCustomPlotWithROI* parentPlot):
    QCPItemRect(reinterpret_cast<QCustomPlot*>(parentPlot)),
    mParentPlotwROI(parentPlot),
    ROIid(0)
    //sizeLabel(parentPlot)
{
    topLeftPos_startOfMove.setX(0);
    topLeftPos_startOfMove.setY(0);
    sizeLabel = new QCPItemText(reinterpret_cast<QCustomPlot*>(parentPlot));
    VLine = new QCPItemLine(reinterpret_cast<QCustomPlot*>(parentPlot));
    HLine = new QCPItemLine(reinterpret_cast<QCustomPlot*>(parentPlot));
    std::cout << "Made a ROI!" << std::endl;
}


QCPRectROI::~QCPRectROI()
{
    parentPlot()->removeItem(sizeLabel);
    parentPlot()->removeItem(VLine);
    parentPlot()->removeItem(HLine);
    std::cout << "ROI destroyed !" << std::endl;
}


void QCPRectROI::mouseMoveEvent(QMouseEvent* event, const QPointF& startPos)
{
    //std::cout << "moving rect!" << std::endl;
    if(selected())
    {
        // calculate the position of topLeft anchor with respect to the start point of the moveEvent
        // posDifference = posTL@endMove - posMouse@endMove = posTL@startMove - posMouse@startMove
        // hence posTL@endMove = posMouse@endMove + posDifference

        // startPos stays the same for a whole succession of mouse movements as long as the
        // left button is not released ! it does not change at each new mouseMoveEvent

        QPointF topLeft_to_bottomRight = bottomRight->coords() - topLeft->coords();
        //std::cout << "topLeft_to_bottomRight:"; print_QPointF(topLeft_to_bottomRight);

        //transform startPos as well as event->localPos() into plot coordinates using the link to
        //the parent QCPwithROI, which is the entity that knows about the relationship between widget pixels
        //and axis coordinates
        double eventKey, eventValue = 0;
        mParentPlotwROI->colorMap->pixelsToCoords(event->localPos(), eventKey, eventValue);
        QPointF eventLocalPos(eventKey, eventValue);

        double startPosKey, startPosValue = 0;
        mParentPlotwROI->colorMap->pixelsToCoords(startPos, startPosKey, startPosValue);
        QPointF startPosCoords(startPosKey, startPosValue);

        QPointF coordsDifference_moveStartToTopLeftBegin = topLeftPos_startOfMove - startPosCoords;

        topLeft->setCoords(eventLocalPos + coordsDifference_moveStartToTopLeftBegin);
        bottomRight->setCoords(topLeft->coords() + topLeft_to_bottomRight);

        //also refresh position of the size label
        QPointF sizePos = bottom->pixelPosition() - label_offset;
        sizeLabel->position->setCoords(sizePos);

        emit(rectangleMoved());
    }
    //std::cout << "top left position -> X:" << topLeft->coords().x() << ", Y:" << topLeft->coords().y() << std::endl;
}


// when the left button is pressed, this event handler registers where move started for the subsequent move
void QCPRectROI::mousePressEvent(QMouseEvent* event, const QVariant& details)
{
    if(selected() && event->button()==Qt::LeftButton)
    {
        topLeftPos_startOfMove.setX(topLeft->coords().x());
        topLeftPos_startOfMove.setY(topLeft->coords().y());
    }
}

//not very pretty but it works quite nicely
void QCPRectROI::sizeChangeFromWheel(QWheelEvent* event, bool vertical, bool bigStep)
{
    int d = event->angleDelta().y();
    QPointF newPtl;
    QPointF newPbr;
    double deltaX = mParentPlotwROI->singleCellStepX();
    double deltaY = mParentPlotwROI->singleCellStepY();

    if (vertical) //vertical case
    {
        if(d>0)
        {
            if(bigStep)
            {
                QPointF Ybigstep{0,10.*deltaY};
                newPtl = topLeft->coords() - Ybigstep;
                newPbr = bottomRight->coords() + Ybigstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
            }
            else
            {
                QPointF Ystep{0,deltaY};
                newPtl = topLeft->coords() - Ystep;
                newPbr = bottomRight->coords() + Ystep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
            }
        }
        else if(d<0)
        {
            if(bigStep)
            {
                if(sizeY()>24.*deltaY)
                {
                QPointF Ybigstep{0,10.*deltaY};
                newPtl = topLeft->coords() + Ybigstep;
                newPbr = bottomRight->coords() - Ybigstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
                }
            }
            else
            {
                if(sizeY()>4.*deltaY)
                {
                    QPointF Ystep{0,deltaY};
                    newPtl = topLeft->coords() + Ystep;
                    newPbr = bottomRight->coords() - Ystep;
                    topLeft->setCoords(newPtl);
                    bottomRight->setCoords(newPbr);
                }
            }
        }
    }
    else //horizontal case
    {
        if(d>0)
        {
            if(bigStep)
            {
                QPointF Xbigstep{10.*deltaX,0};
                newPtl = topLeft->coords() - Xbigstep;
                newPbr = bottomRight->coords() + Xbigstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
            }
            else
            {
                QPointF Xstep{deltaX,0};
                newPtl = topLeft->coords() - Xstep;
                newPbr = bottomRight->coords() + Xstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
            }
        }
        else if(d<0)
        {
            if(bigStep)
            {
                if(sizeX()>24.*deltaX)
                {
                QPointF Xbigstep{10.*deltaX,0};
                newPtl = topLeft->coords() + Xbigstep;
                newPbr = bottomRight->coords() - Xbigstep;
                topLeft->setCoords(newPtl);
                bottomRight->setCoords(newPbr);
                }
            }
            else
            {
                if(sizeX()>4.*deltaX)
                {
                    QPointF Xstep{deltaX,0};
                    newPtl = topLeft->coords() + Xstep;
                    newPbr = bottomRight->coords() - Xstep;
                    topLeft->setCoords(newPtl);
                    bottomRight->setCoords(newPbr);
                }
            }
        }
    }

    //then update the size label
    int sizeXint = roundToInt(sizeX()/mParentPlotwROI->singleCellStepX());
    int sizeYint = roundToInt(sizeY()/mParentPlotwROI->singleCellStepY());
    std::string sizestr = "ROI " + std::to_string(ROIid) + ": " + std::to_string(sizeXint) + ", " + std::to_string(sizeYint);
    sizeLabel->setText(QString::fromStdString(sizestr));
    QPointF sizepos = bottom->pixelPosition() - label_offset;
    sizeLabel->position->setCoords(sizepos);
}

