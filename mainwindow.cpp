#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lodepng.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    nx(400),
    ny(400)
{
    ui->setupUi(this);
    keyVec.reserve(nx);
    valueVec.reserve(ny);

    ui->picsList->setViewMode(QListView::IconMode);
    ui->picsList->setIconSize(QSize(200,200));
    ui->picsList->setResizeMode(QListView::Adjust);

    ui->horizontalLine->addGraph();
    ui->horizontalLine->graph(0)->setPen(QPen(Qt::blue));

    ui->verticalLine->addGraph(ui->verticalLine->yAxis, ui->verticalLine->xAxis);
    ui->verticalLine->graph(0)->setPen(QPen(Qt::green));

    QString dir{"/home/jojo/Pictures/"};
    QString filename = dir;

    filename += "lion.png";
    QListWidgetItem* lion = new QListWidgetItem(QIcon(filename),filename);
    Qt::ItemFlags currentFlags = lion->flags();
    lion->setFlags(currentFlags&(~Qt::ItemIsDragEnabled));
    ui->picsList->addItem(lion);

    filename = dir;
    filename += "church.png";
    QListWidgetItem* tiger = new QListWidgetItem(QIcon(filename),filename);
    tiger->setFlags(currentFlags&(~Qt::ItemIsDragEnabled));
    ui->picsList->addItem(tiger);

    filename = dir;
    filename += "car.png";
    QListWidgetItem* car = new QListWidgetItem(QIcon(filename),filename);
    car->setFlags(currentFlags&(~Qt::ItemIsDragEnabled));
    ui->picsList->addItem(car);

    filename = dir;
    filename += "greyscale_blobs.png";
    QListWidgetItem* blobs = new QListWidgetItem(QIcon(filename),filename);
    blobs->setFlags(currentFlags&(~Qt::ItemIsDragEnabled));
    ui->picsList->addItem(blobs);

    connect(ui->myPlot, SIGNAL(posMovedToX(double)), ui->XstageLCD, SLOT(display(double)));
    connect(ui->myPlot, SIGNAL(posMovedToY(double)), ui->YstageLCD, SLOT(display(double)));
    connect(ui->picsList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(loadPicture(QListWidgetItem*)));
}


void MainWindow::fillKeyVec()
{
    keyVec.clear();
    double key;
    for (int i = 0; i<nx;++i)
    {
        ui->myPlot->colorMap->data()->cellToCoord(i, 1, &key, 0);
        keyVec.push_back(key);
    }
}

void MainWindow::fillValueVec()
{
    valueVec.clear();
    double value;
    for (int j = 0; j<ny;++j)
    {
        ui->myPlot->colorMap->data()->cellToCoord(1, j, 0, &value);
        valueVec.push_back(value);
    }
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addROI_clicked()
{
    int n = ui->myPlot->ROInumber();

    double xstep = ui->myPlot->singleCellStepX();
    double ystep = ui->myPlot->singleCellStepY();
    double Xcenter = ui->myPlot->colorMap->data()->keyRange().lower + (nx/5)*xstep + (n%4)*(nx/5)*xstep;
    double Ycenter = ui->myPlot->colorMap->data()->valueRange().lower + (ny/5)*ystep + (n/4)*(nx/5)*ystep;

    QPointF center{Xcenter, Ycenter};
    ui->myPlot->addROI(center, nx/10, ny/10);
    ui->myPlot->replot();
}

void MainWindow::on_removeROI_clicked()
{
    ui->myPlot->removeROI();
    ui->myPlot->replot();
}

double function_to_plot(double x, double y)
{
    double r = qSqrt(x*x + y*y);
    double z, a;
    z = qSin(r)/(1+qSqrt(r));
//    if(r != 0.)
//    {
//        a = qSin(6*x)/qSin(x)*qSin(6*y)/qSin(y);
//        z = a*a;
//    }
//    else
//        z = 0;
    return z;
}

void MainWindow::enableCrosshair()
{
    ui->myPlot->enableCrosshair();
    connect(ui->myPlot->lineH, SIGNAL(lineMoved()), this, SLOT(replotAlongHLine()));
    connect(ui->myPlot->lineV, SIGNAL(lineMoved()), this, SLOT(replotAlongVLine()));
}

void MainWindow::disableCrosshair()
{
    ui->myPlot->disableCrosshair();
}

QVector<double> MainWindow::getVData()
{
    QVector<double> yVec;
    // need to get the cell index (from 0 to keySize-1 ) corresponding to the coordinate position of the vertical line
    // first get the graph coordinates (i.e. a double) from point1 of the vertical line, then get the cell number of that point
    //double key, value;
    int cellPos_VLine;
    QPointF Pt1 {ui->myPlot->lineV->point1->coords()};
    //ui->myPlot->colorMap->pixelsToCoords(ui->myPlot->lineV->point1->coords(), key, value);
    ui->myPlot->colorMap->data()->coordToCell(Pt1.x(), Pt1.y(), &cellPos_VLine, 0);
    //fill the vector with all the cell values along the vertical line
    for (int i = 0; i < ny; ++i)
    {
        yVec.push_back(ui->myPlot->colorMap->data()->cell(cellPos_VLine, i));
    }
    return yVec;
}


QVector<double> MainWindow::getHData()
{
    QVector<double> xVec;
    // need to get the index (from 0 to valueSize -1 ) corresponding to the pixel position of the horizontal line
    // first get the graph coordinates (i.e. a double) from point1 of the horizontal line, then get the cell number of that point
    //double key, value;
    int cellPos_HLine;
    QPointF Pt1{ui->myPlot->lineH->point1->coords()};
    //ui->myPlot->colorMap->pixelsToCoords(ui->myPlot->lineH->point1->coords(), key, value);
    ui->myPlot->colorMap->data()->coordToCell(Pt1.x(), Pt1.y(), 0, &cellPos_HLine);
    //fill the vector with all the cell values along the horizontal line
    for (int i = 0; i < nx; ++i)
    {
        xVec.push_back(ui->myPlot->colorMap->data()->cell(i, cellPos_HLine));
    }
    return xVec;
}

void MainWindow::replotAlongHLine()
{
    QVector<double> HData = getHData();
    double Hmax = *std::max_element(HData.constBegin(), HData.constEnd());
    double Hmin = *std::min_element(HData.constBegin(), HData.constEnd());
    ui->horizontalLine->yAxis->setRange(Hmin, Hmax);
    ui->horizontalLine->graph(0)->setData(keyVec, HData);
    ui->horizontalLine->replot();
}

void MainWindow::replotAlongVLine()
{
    QVector<double> VData = getVData();
    double Vmax = *std::max_element(VData.constBegin(), VData.constEnd());
    double Vmin = *std::min_element(VData.constBegin(), VData.constEnd());
    ui->verticalLine->xAxis->setRange(Vmin, Vmax);
    ui->verticalLine->graph(0)->setData(valueVec, VData);
    ui->verticalLine->replot();
}


void MainWindow::plotCMap(int coeff)
{
    // configure axis rect:
    ui->myPlot->axisRect()->setupFullAxesBox(true);
    ui->myPlot->xAxis->setLabel("x");
    ui->myPlot->yAxis->setLabel("y");

    ui->myPlot->colorMap->data()->setSize(nx, ny); // we want the color map to have nx * ny data points
    ui->myPlot->colorMap->data()->setRange(QCPRange(-4, 4), QCPRange(-4, 4)); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions
    // now we assign some data, by accessing the QCPColorMapData instance of the color map:
    double x, y, z;
    for (int xIndex=0; xIndex<nx; ++xIndex)
    {
      for (int yIndex=0; yIndex<ny; ++yIndex)
      {
        ui->myPlot->colorMap->data()->cellToCoord(xIndex, yIndex, &x, &y);
        z = function_to_plot(coeff*x,coeff*y);
        ui->myPlot->colorMap->data()->setCell(xIndex, yIndex, z);
      }
    }
    fillKeyVec();
    fillValueVec();

    // set the color gradient of the color map to one of the presets:
    ui->myPlot->colorMap->setGradient(QCPColorGradient::gpPolar);

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    ui->myPlot->colorMap->rescaleDataRange();
    ui->horizontalLine->xAxis->setRange(ui->myPlot->colorMap->data()->keyRange());
    ui->verticalLine->yAxis->setRange(ui->myPlot->colorMap->data()->valueRange());

    // rescale the key (x) and value (y) axes so the whole color map is visible:
    ui->myPlot->colorMap->setInterpolate(false);
    ui->myPlot->rescaleAxes();

}


void MainWindow::loadPicture(QListWidgetItem* item)
{
    QMessageBox warningBox;
    warningBox.setText("Go to center of image or display image ?");
    warningBox.setIcon(QMessageBox::Warning);
    QPushButton* gotoButton = warningBox.addButton("Go to center", QMessageBox::AcceptRole);
    QPushButton* displayButton = warningBox.addButton("Only display", QMessageBox::AcceptRole);
    warningBox.exec();

    std::vector<unsigned char> image; //the raw pixels
    unsigned int width, height;
    //get filename from the clicked item
    QString QtFilename = item->text();
    std::cout << "icon name:" << QtFilename.toStdString() << std::endl;
    //decode the png file
    unsigned int error = lodepng::decode(image, width, height, QtFilename.toStdString().c_str());
    //if there's an error, display it
    if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
    std::cout << "width is " << width << ", height is " << height << std::endl;

    nx = width;
    ny = height;
    // configure axis rect:
    ui->myPlot->axisRect()->setupFullAxesBox(true);
    ui->myPlot->xAxis->setLabel("x");
    ui->myPlot->yAxis->setLabel("y");

    //remove ROIs before loading new image
    while(ui->myPlot->ROIStore.size()>0)
    {
        ui->myPlot->removeROI();
    }

    ui->myPlot->colorMap->data()->setSize(width, height); // we want the color map to have nx * ny data points
    ui->myPlot->colorMap->data()->setRange(QCPRange(0, (double)width), QCPRange(0, (double)height)); // and span the coordinate range width in key (x) and height in value (y)
    //ui->horizontalPlot->xAxis->setRange(0,(double)width);
    //ui->verticalPlot->yAxis->setRange(0,(double)height);

    double z;
    for (int xIndex=0; xIndex<width; ++xIndex)
    {
      for (int yIndex=0; yIndex<height; ++yIndex)
      {
        //one line is 4*width long in the std::vector<unsigned char>
        z = (double)image[4*(yIndex*width+xIndex)];
        // PNG image is specified left-to-right & top-to-bottom whereas natural axis direction is left-to-right & bottom-to-top
        ui->myPlot->colorMap->data()->setCell(xIndex, height-1-yIndex, z);
      }
    }
    //prepare the vectors of key and values as well as the ranges for plotting along the crosshair lines
    fillKeyVec();
    fillValueVec();

    // set the color gradient of the color map to one of the presets:
    ui->myPlot->colorMap->setGradient(QCPColorGradient::gpGrayscale);

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    ui->myPlot->colorMap->rescaleDataRange();
    // rescale the key (x) and value (y) axes so the whole color map is visible:
    ui->myPlot->colorMap->setInterpolate(false);
    ui->myPlot->rescaleAxes();


    //set the position tracer and "You are here" sign to visible
    //then update the put the position tracer in the middle of the new image
    if(warningBox.clickedButton()==gotoButton)
    {
        if (!(ui->myPlot->stagePosition->visible()))
        {
            ui->myPlot->stagePosition->setVisible(true);
        }
        if (!(ui->myPlot->YouAreHere->visible()))
        {
            ui->myPlot->YouAreHere->setVisible(true);
        }

        double centerX = ui->myPlot->xAxis->range().center();
        double centerY = ui->myPlot->yAxis->range().center();
        //std::cout << "x center is " << centerX << ", y center is" << centerY << std::endl;
        ui->myPlot->stagePosition->position->setCoords(centerX, centerY);
        QPointF middle = ui->myPlot->stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
        ui->myPlot->YouAreHere->position->setCoords(middle);
        ui->myPlot->setVisible(true);

        emit(ui->myPlot->posMovedToX(centerX));
        emit(ui->myPlot->posMovedToY(centerY));
    }
    //prepare the key ranges for plotting along the crosshair lines
    ui->horizontalLine->xAxis->setRange(ui->myPlot->colorMap->data()->keyRange());
    ui->verticalLine->yAxis->setRange(ui->myPlot->colorMap->data()->valueRange());

    //remove the crosshair
    if(ui->crossHairBox->checkState())
    {
        disableCrosshair();
        ui->crossHairBox->setCheckState(Qt::CheckState::Unchecked);
    }
    //eventually plot the loaded image
    ui->myPlot->replot();

}


void MainWindow::on_crossHairBox_stateChanged(int newState)
{
    if(newState)
        enableCrosshair();
    else if (ui->myPlot->lineH != nullptr && ui->myPlot->lineV != nullptr)
        disableCrosshair();
}


void MainWindow::on_goToRoiButton_clicked()
{
    int i = ui->ROInumber->value();
    if (i<1 || i>(ui->myPlot->ROInumber()))
    {
        std::cout << "ROI index invalid !!!" << std::endl;
        return;
    }
    else if (ui->myPlot->ROInumber() == 0)
    {
        std::cout << "No ROI to go to !!!" << std::endl;
        return;
    }
    ui->myPlot->goToROI(i);
}


void MainWindow::on_resetAxes_clicked()
{
    ui->myPlot->xAxis->setRange(ui->myPlot->colorMap->data()->keyRange());
    ui->myPlot->yAxis->setRange(ui->myPlot->colorMap->data()->valueRange());

    //keep position of "you are here !" sign with respect to ROI
    if (ui->myPlot->YouAreHere->visible())
    {
        QPointF here = ui->myPlot->stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
        ui->myPlot->YouAreHere->position->setCoords(here);
    }
    //keep position of size label with respect to the ROI
    for (auto ROI : ui->myPlot->ROIStore)
    {
        QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
        ROI->sizeLabel->position->setCoords(sizepos);
    }

    ui->myPlot->replot();
}
