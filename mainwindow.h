#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "qcustomplot.h"
#include <iostream>

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void plotCMap(int coeff);
    void enableCrosshair();
    void disableCrosshair();
    QVector<double> getHData();
    QVector<double> getVData();
    void fillKeyVec();
    void fillValueVec();


private slots:
    void on_addROI_clicked();
    void on_removeROI_clicked();
    void on_crossHairBox_stateChanged(int arg1);
    void on_goToRoiButton_clicked();
    void loadPicture(QListWidgetItem* item);
    void replotAlongHLine();
    void replotAlongVLine();

    void on_resetAxes_clicked();

private:
    Ui::MainWindow *ui;

    //QCPColorMap* myColorMap;
    unsigned int nx;
    unsigned int ny;
    QVector<double> keyVec;
    QVector<double> valueVec;
};

#endif // MAINWINDOW_H
